#define _CRT_SECURE_NO_WARNINGS
#include "EntryH.h"
#include <Windows.h>

CRITICAL_SECTION EHCSection;

// Pronalazi entry po proslijedjenom broju i smjesta privremeno u memoriju
bool fsImpl::EntryHandler::loadByNumber (int num) {

	EnterCriticalSection (&EHCSection);

	int
		i = 0,
		j = 0;

	number = num;

	while (myDirCluster.loadCluster (i) == true) {

		for (int k = 0; k < 102; k++) {

			if (myDirCluster.entries[k].name[0] == 0) {

				if (myDirCluster.entries[k].reserved == 0) {

					LeaveCriticalSection (&EHCSection);
					return false;

				}

			} else {

				if (j == num) {

					offset = k;
					LeaveCriticalSection (&EHCSection);
					return true;

				}

				j++;

			}

		}

		i++;

	}

	LeaveCriticalSection (&EHCSection);
	return false;

}

// Pronalazi entry po proslijedjenom imenu i smjesta privremeno u memoriju
bool fsImpl::EntryHandler::loadByName (std::string name) {

	EnterCriticalSection (&EHCSection);

	int
		i = 0;

	number = -1;

	while (myDirCluster.loadCluster (i) == true) {

		for (int k = 0; k < 102; k++) {

			if (myDirCluster.entries[k].name == name) {

				offset = k;
				LeaveCriticalSection (&EHCSection);
				return true;

			}

		}

		i++;

	}

	LeaveCriticalSection (&EHCSection);
	return false;

}

// Pronalazi sledeci entry i smjesta privremeno u memoriju
bool fsImpl::EntryHandler::loadNext () {

	if (number == -1) { return false; }
	number++;

	while (offset < 102) {

		offset++;

		if (myDirCluster.entries[offset].name[0] == 0) {

			if (myDirCluster.entries[offset].reserved == 0) { return false; }

		} else { return	true; }

	}

	return loadByNumber (number);

}

// Smjesta direktorijumski cluster na disk
void fsImpl::EntryHandler::save () {

	EnterCriticalSection (&EHCSection);
	Entry tmpE = getReference ();
	myDirCluster.part->readCluster (myDirCluster.location, (char *)myDirCluster.entries);
	getReference () = tmpE;
	myDirCluster.storeCluster ();
	LeaveCriticalSection (&EHCSection);

}

// Resetuje entry pri otvaranju fajla u "w" modu
void resetEntry (Entry& entry, std::string name) {

	std::strcpy (entry.name, name.c_str ());
	entry.indexCluster = 0;
	entry.size = 0;

}

// Kreira novi entry u direktorijumu
void fsImpl::EntryHandler::create (std::string name) {

	EnterCriticalSection (&EHCSection);

	int
		i = 0;

	number = -1;

	while (myDirCluster.loadCluster (i) == true) {

		for (int k = 0; k < 102; k++) {

			if (myDirCluster.entries[k].name[0] == 0) {

				offset = k;
				resetEntry (myDirCluster.entries[k], name);
				LeaveCriticalSection (&EHCSection);
				save ();
				return;

			}

		}

		i++;

	}

	myDirCluster.appendCluster ();
	offset = 0;
	resetEntry (myDirCluster.entries[0], name);
	LeaveCriticalSection (&EHCSection);
	save ();

}

// Brise entry ali ne i podatke
void fsImpl::EntryHandler::remove () {

	EnterCriticalSection (&EHCSection);
	for (char &x : myDirCluster.entries[offset].name) { x = 0; }
	for (char &x : myDirCluster.entries[offset].ext) { x = 0; }
	myDirCluster.entries[offset].reserved = 1;
	LeaveCriticalSection (&EHCSection);
	save ();

}

// Vraca referencu na oznaceni entry
Entry & fsImpl::EntryHandler::getReference () {

	return myDirCluster.entries[offset];

}
