#include "KernelFS.h"
#include "file.h"
#include "KernelFile.h"
#include "FileH.h"
#include "EntryH.h"
#include "BitVector.h"

KernelFS::KernelFS () {

	for (Partition* &x : letters) { x = nullptr; }

}

KernelFS::~KernelFS () {}

char KernelFS::mount (Partition * partition) {

	for (int i = 0; i < 26; i++) {

		if (letters[i] == nullptr) {

			letters[i] = partition;
			bitVector[i] = new fsImpl::BitVector (partition);
			InitializeCriticalSection (partBusy + i);
			partStates[i] = ENABLED;

			return 'A' + i;

		}

	}

	return '0';

}

char KernelFS::unmount (char part) {

	int
		partNo = part - 'A';

	if (letters[partNo] == nullptr) { return 0; }

	partStates[partNo] = DISABLED;

	EnterCriticalSection (partBusy + partNo);

	bitVector[partNo]->flush ();
	delete bitVector[partNo];
	letters[partNo] = nullptr;

	DeleteCriticalSection (partBusy + partNo);
	return 1;

}

char KernelFS::format (char part) {

	int
		partNo = part - 'A';

	if (letters[partNo] == 0) { return 0; }

	partStates[partNo] = DISABLED;
	EnterCriticalSection (partBusy + partNo);

	bitVector[partNo]->init ();
	bitVector[partNo]->reserveNext ();
	bitVector[partNo]->flush ();

	char
		zeroBuffer[ClusterSize] = {};

	letters[partNo]->writeCluster (bitVector[partNo]->size, zeroBuffer);

	partStates[partNo] = ENABLED;
	LeaveCriticalSection (partBusy + partNo);
	return 1;

}

char KernelFS::readRootDir (char part, EntryNum n, Directory & d) {

	int
		partNo = part - 'A',
		i = 1;

	fsImpl::EntryHandler entryHandle;
	entryHandle.myDirCluster.bitVec = bitVector[partNo];
	entryHandle.myDirCluster.part = letters[partNo];

	if (entryHandle.loadByNumber (n) == false) { return 0; }

	d[0] = entryHandle.getReference ();

	while (entryHandle.loadNext () == true) {

		if (i == 64) { return 65; }

		d[i] = entryHandle.getReference ();
		i++;
	}

	return i;

}

char KernelFS::doesExist (char * fname) {

	std::string
		name = convertName (fname);

	fsImpl::EntryHandler entryHandle;
	entryHandle.myDirCluster.bitVec = bitVector[*fname - 'A'];
	entryHandle.myDirCluster.part = letters[*fname - 'A'];

	return entryHandle.loadByName (name);

}

File * KernelFS::open (char * fname, char mode) {

	int partNo = *fname - 'A';
	if (partStates[partNo] == DISABLED) { return nullptr; }

	File* filePtr = new File;
	filePtr->myImpl->myFS = this;
	filePtr->myImpl->absolutePath = fname;
	filePtr->myImpl->upToDate = true;
	filePtr->myImpl->fileHand.bitVec = bitVector[partNo];
	filePtr->myImpl->fileHand.entHandler.myDirCluster.bitVec = bitVector[partNo];
	filePtr->myImpl->fileHand.entHandler.myDirCluster.part = letters[partNo];
	filePtr->myImpl->fileHand.part = letters[partNo];

	switch (mode) {

	case 'r':

		filePtr->myImpl->mode = 'r';

		if (filePtr->myImpl->fileHand.entHandler.loadByName (convertName (fname)) == false) {

			filePtr->myImpl->mode = 'x';
			delete filePtr;
			filePtr = nullptr;

		} else {

			filePtr->myImpl->seek (0);
		}

		break;

	case 'w':

		filePtr->myImpl->mode = 'w';

		if (filePtr->myImpl->fileHand.entHandler.loadByName (convertName (fname)) == true) {

			filePtr->myImpl->seek (0);
			filePtr->myImpl->truncate ();
			filePtr->myImpl->fileHand.appendCluster ();

		} else {

			filePtr->myImpl->fileHand.entHandler.create (convertName (fname));
			filePtr->myImpl->fileHand.appendCluster ();
			filePtr->myImpl->seek (0);

		}

		break;

	case 'a':

		filePtr->myImpl->mode = 'a';

		if (filePtr->myImpl->fileHand.entHandler.loadByName (convertName (fname)) == true) {

			filePtr->myImpl->seek (filePtr->getFileSize ());

		} else {

			filePtr->myImpl->mode = 'x';
			delete filePtr;
			filePtr = nullptr;

		}

		break;

	}

	if (filePtr != nullptr) {

		if (numOfOpened[partNo] == 0) { EnterCriticalSection (partBusy + partNo); }

		numOfOpened[partNo]++;

	}

	return filePtr;

}

char KernelFS::deleteFile (char * fname) {

	int partNo = *fname - 'A';
	if (openedFiles[partNo].find (fname) != openedFiles[partNo].end ()) {

		if (TryAcquireSRWLockExclusive (&openedFiles[partNo][fname]) == 0) { return 0; }

	}

	std::string
		name = convertName (fname);

	fsImpl::FileHandler fHand;
	fHand.entHandler.myDirCluster.bitVec = bitVector[partNo];
	fHand.bitVec = bitVector[partNo];
	fHand.entHandler.myDirCluster.part = letters[partNo];
	fHand.part = letters[partNo];
	fHand.entHandler.loadByName (name);

	int i = 0;
	while (fHand.freeCluster (i++));

	bitVector[partNo]->release (fHand.entHandler.getReference ().indexCluster);
	fHand.entHandler.remove ();
	return 1;

}

std::string KernelFS::convertName (char * fname) {
	std::string
		name = "           ";

	for (int i = 3, j = 0; j < 11 && fname[i] != 0; i++) {

		if (fname[i] != '.') {

			name[j] = fname[i];
			j++;

		} else { j = 8; }

	}

	return name;

}
