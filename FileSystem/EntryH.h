#pragma once
#include "DirectoryH.h"
#include <string>

class FileHandler;
class KernelFS;

namespace fsImpl {

	class FileHandler;

	class EntryHandler {

		EntryHandler () {};
		bool loadByNumber (int num);
		bool loadByName (std::string name);
		bool loadNext ();
		void save ();
		void create (std::string name);
		void remove ();
		Entry& getReference ();

		int	number;
		DirectoryHandler myDirCluster;
		int	offset;

		friend class KernelFS;
		friend class KernelFile;
		friend class FileHandler;

	};

}
