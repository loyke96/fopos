#pragma once
#include "EntryH.h"

namespace fsImpl {

	class FileHandler {

		FileHandler () {};
		bool loadCluster (int num);
		void storeCluster ();
		void appendCluster ();
		bool freeCluster (int num);

		EntryHandler entHandler;
		char storedData[ClusterSize];
		ClusterNo location;
		BitVector* bitVec;
		Partition* part;

		friend class KernelFile;
		friend class KernelFS;

	};

}
