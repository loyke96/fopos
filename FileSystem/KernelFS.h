#pragma once
#include "FileH.h"
#include <unordered_map>
#include <Windows.h>

class KernelFS {

	enum PartitionState {
		DISABLED, ENABLED
	};

	friend class FS;
	friend class KernelFile;

	KernelFS ();
	~KernelFS ();
	char mount (Partition* partition);
	char unmount (char part);
	char format (char part);
	char readRootDir (char part, EntryNum n, Directory &d);
	char doesExist (char* fname);
	File* open (char* fname, char mode);
	char deleteFile (char* fname);
	std::string convertName (char* fname);

	Partition* letters[26];
	PartitionState partStates[26];
	fsImpl::BitVector* bitVector[26];
	std::unordered_map<std::string, SRWLOCK> openedFiles[26];
	unsigned numOfOpened[26] = {};
	CRITICAL_SECTION partBusy[26];

};
