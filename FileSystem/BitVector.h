#pragma once
#include "part.h"

class KernelFS;
class KernelFile;

namespace fsImpl {

	class FileHandler;
	class DirectoryHandler;
	class EntryHandler;

	class BitVector {
		BitVector (Partition* partition);
		~BitVector ();

		ClusterNo reserveNext ();
		void release (ClusterNo cluster);
		void flush ();
		void init ();

		Partition* myPartition;
		ClusterNo size;
		unsigned char *cache;

		friend class DirectoryHandler;
		friend class KernelFS;
		friend class FileHandler;

	};

}
