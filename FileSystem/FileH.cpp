#include "FileH.h"
#include "BitVector.h"

// Ucitava cluster sa diska u cache
bool fsImpl::FileHandler::loadCluster (int num) {

	if (entHandler.getReference ().indexCluster == 0) { return false; }

	char
		firstLevel[ClusterSize],
		secondLevel[ClusterSize];

	part->readCluster (entHandler.getReference ().indexCluster, firstLevel);

	if (num >= (ClusterSize / 8)) {

		int
			fl = (num - 256) / 512 + 256,
			sl = (num - 256) % 512;

		location = ((ClusterNo*)firstLevel)[fl];

		if (location == 0) { return false; }

		part->readCluster (location, secondLevel);
		location = ((ClusterNo*)secondLevel)[sl];

		if (location == 0) { return false; }

		part->readCluster (location, storedData);

	} else {

		location = ((ClusterNo*)firstLevel)[num];

		if (location == 0) { return false; }

		part->readCluster (location, storedData);

	}

	return true;

}

// Smjesta cluster iz cachea na disk
void fsImpl::FileHandler::storeCluster () {

	part->writeCluster (location, storedData);

}

// Dodaje novi cluster u fajl sa ucitavanjem u cache
void fsImpl::FileHandler::appendCluster () {

	char
		clusterBuffer[ClusterSize],
		zeroSpace[ClusterSize] = {};

	if (entHandler.getReference ().indexCluster == 0) {

		entHandler.getReference ().indexCluster = bitVec->reserveNext ();
		part->writeCluster (entHandler.getReference ().indexCluster, zeroSpace);

	}

	location = bitVec->reserveNext ();
	std::memcpy (storedData, zeroSpace, ClusterSize);
	part->readCluster (entHandler.getReference ().indexCluster, clusterBuffer);

	int
		i = 0;

	for (; ((ClusterNo*)clusterBuffer)[i] != 0; i++);

	if (i > 256) {

		char
			secondLevelBuffer[ClusterSize];

		part->readCluster (((ClusterNo*)clusterBuffer)[i - 1], secondLevelBuffer);

		for (int j = 0; j < 512; j++) {

			if (((ClusterNo*)secondLevelBuffer)[j] == 0) {

				((ClusterNo*)secondLevelBuffer)[j] = location;
				part->writeCluster (((ClusterNo*)clusterBuffer)[i - 1], secondLevelBuffer);
				return;

			}

		}

	}

	if (i >= 256) {

		*(ClusterNo*)zeroSpace = location;
		((ClusterNo*)clusterBuffer)[i] = bitVec->reserveNext ();
		part->writeCluster (((ClusterNo*)clusterBuffer)[i], zeroSpace);
		part->writeCluster (entHandler.getReference ().indexCluster, clusterBuffer);
		return;

	}

	((ClusterNo*)clusterBuffer)[i] = location;
	part->writeCluster (entHandler.getReference ().indexCluster, clusterBuffer);
	return;

}

// Oslobadja cluster
bool fsImpl::FileHandler::freeCluster (int num) {

	char
		firstLevel[ClusterSize],
		secondLevel[ClusterSize];

	part->readCluster (entHandler.getReference ().indexCluster, firstLevel);

	if (num >= (ClusterSize / 8)) {

		int
			fl = (num - 256) / 512 + 256,
			sl = (num - 256) % 512;

		location = ((ClusterNo*)firstLevel)[fl];

		if (location == 0) { return false; }

		part->readCluster (location, secondLevel);
		location = ((ClusterNo*)secondLevel)[sl];
		if (location == 0) { return false; }
		((ClusterNo*)secondLevel)[sl] = 0;
		part->writeCluster (((ClusterNo*)firstLevel)[fl], secondLevel);
		bitVec->release (location);

		for (int i = 0; i < ClusterSize / 4; i++) {

			if (((ClusterNo*)secondLevel)[i] != 0) { return true; }

		}

		bitVec->release (((ClusterNo*)firstLevel)[fl]);
		((ClusterNo*)firstLevel)[fl] = 0;
		part->writeCluster (entHandler.getReference ().indexCluster, firstLevel);

	} else {

		location = ((ClusterNo*)firstLevel)[num];

		if (location == 0) { return false; }
		((ClusterNo*)firstLevel)[num] = 0;
		part->writeCluster (entHandler.getReference ().indexCluster, firstLevel);
		bitVec->release (location);

	}

	return true;

}
