#include "KernelFile.h"
#include "KernelFS.h"

KernelFile::~KernelFile () {

	if (mode == 'x') { return; }

	int partNo = absolutePath[0] - 'A';

	auto& srwlock = myFS->openedFiles[partNo][absolutePath];
	myFS->numOfOpened[partNo]--;

	switch (mode) {

	case 'r':

		ReleaseSRWLockShared (&srwlock);
		break;

	case 'w': case 'a':

		if (upToDate == false) { fileHand.storeCluster (); }
		fileHand.entHandler.save ();
		ReleaseSRWLockExclusive (&srwlock);
		break;

	}

	if (myFS->numOfOpened[partNo] == 0) { LeaveCriticalSection (myFS->partBusy + partNo); }

}

char KernelFile::write (BytesCnt size, char * buffer) {

	if (mode == 'r') { return 0; }

	int writeCount = 0;

	if (fileHand.entHandler.getReference ().size < position + size) {

		fileHand.entHandler.getReference ().size = position + size;

	}

	while (size > 0) {

		unsigned bufferLoad = ClusterSize - (position % ClusterSize);

		if (bufferLoad > size) { bufferLoad = size; }

		std::memcpy (fileHand.storedData + (position%ClusterSize), buffer + writeCount, bufferLoad);
		position += bufferLoad;
		writeCount += bufferLoad;
		size -= bufferLoad;
		upToDate = false;

		if (position % ClusterSize == 0) {

			fileHand.storeCluster ();
			upToDate = true;

			if (fileHand.loadCluster (position / ClusterSize) == false) {

				fileHand.appendCluster ();

			}
		}
	}

	return size == 0;

}

BytesCnt KernelFile::read (BytesCnt size, char * buffer) {

	BytesCnt readCount = 0;

	if (size > fileHand.entHandler.getReference ().size - position) {

		size = fileHand.entHandler.getReference ().size - position;

	}

	while (readCount < size) {

		unsigned bufferLoad = ClusterSize - (position % ClusterSize);

		if (bufferLoad > size - readCount) { bufferLoad = size - readCount; }

		std::memcpy (buffer + readCount, fileHand.storedData + (position % ClusterSize), bufferLoad);
		position += bufferLoad;
		readCount += bufferLoad;

		if (readCount < size || (position % ClusterSize) == 0) {

			fileHand.loadCluster (position / ClusterSize);

		}

	}

	return size;

}

char KernelFile::seek (BytesCnt pos) {

	if (pos > fileHand.entHandler.getReference ().size) { return 0; }

	if (upToDate == false) { fileHand.storeCluster (); }
	fileHand.loadCluster (pos / ClusterSize);
	position = pos;
	return 1;

}

BytesCnt KernelFile::filePos () {

	return position;

}

char KernelFile::eof () {

	return position == fileHand.entHandler.getReference ().size;

}

BytesCnt KernelFile::getFileSize () {

	return fileHand.entHandler.getReference ().size;

}

char KernelFile::truncate () {

	if (mode == 'r') { return 0; }

	int tempPos = position;

	int toDelete = (tempPos + ClusterSize - 1) / ClusterSize;
	while (fileHand.freeCluster (toDelete++));

	fileHand.entHandler.getReference ().size = position;
	return 1;

}
