#include "BitVector.h"
#include <cmath>
#include <stdexcept>
#include <Windows.h>

CRITICAL_SECTION BVCSection;

fsImpl::BitVector::BitVector (Partition* partition) :myPartition (partition) {

	EnterCriticalSection (&BVCSection);
	size = std::ceil (myPartition->getNumOfClusters () / (ClusterSize*8.0));
	cache = new unsigned char[size*ClusterSize];

	for (ClusterNo i = 0; i < size; i++) {

		myPartition->readCluster (i, (char*)cache + i * ClusterSize);

	}
	LeaveCriticalSection (&BVCSection);

}

fsImpl::BitVector::~BitVector () {

	EnterCriticalSection (&BVCSection);
	flush ();
	delete[] cache;
	LeaveCriticalSection (&BVCSection);

}

// Rezervise prvo slobodno mjesto na disku
ClusterNo  fsImpl::BitVector::reserveNext () {

	EnterCriticalSection (&BVCSection);

	for (unsigned i = 0; i < size*ClusterSize; i++) {

		if (cache[i] != 0xFF) {

			for (int j = 0;; j++) {

				if ((cache[i] >> j) % 2 == 0) {

					cache[i] ^= (1 << j);
					LeaveCriticalSection (&BVCSection);
					return i * 8 + j;

				}

			}

		}

	}

	throw std::overflow_error ("Disk is full.");

}


// Markira cluster kao slobodan
void fsImpl::BitVector::release (ClusterNo cluster) {

	EnterCriticalSection (&BVCSection);

	int
		i = cluster / 8,
		j = cluster % 8;

	cache[i] ^= (1 << j);

	LeaveCriticalSection (&BVCSection);

}

// Flushuje bitVector na disk
void fsImpl::BitVector::flush () {

	EnterCriticalSection (&BVCSection);

	for (ClusterNo i = 0; i < size; i++) {

		myPartition->writeCluster (i, (char*)cache + i * ClusterSize);

	}

	LeaveCriticalSection (&BVCSection);

}

// Resetuje bitVector
void fsImpl::BitVector::init () {

	EnterCriticalSection (&BVCSection);

	for (unsigned i = 0; i < size*ClusterSize; i++) { cache[i] = 0; }

	for (unsigned i = 0; i < size; i++) { reserveNext (); }

	LeaveCriticalSection (&BVCSection);

}
