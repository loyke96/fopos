﻿#pragma once

typedef unsigned long BytesCnt;
typedef unsigned long EntryNum;

const unsigned long ENTRYCNT = 64;
const unsigned int FNAMELEN = 8;
const unsigned int FEXTLEN = 3;

struct Entry {
	char name[FNAMELEN];
	char ext[FEXTLEN];
	char reserved;
	unsigned long indexCluster;
	unsigned long size;
};

typedef Entry Directory[ENTRYCNT];

class KernelFS;
class Partition;
class File;

class FS {
public:
	~FS (); // montiranje particije, vraca dodjeljeno slovo
	static char mount (Partition* partition); // demontira particiju oznacenu datim slovom. Vraca
											 // 0 u slucaju neuspjeha ili 1 u slucaju uspjeha
	static char unmount (char part); // particija zadata slovom se formatira. Vraca 0 u slucaju neuspjeha ili 1 u slucaju uspjeha
	static char format (char part); //prvim argumentom se zadaje particija, drugim redni
								   // broj validnog ulaza od kog se počinje čitanje
	static char readRootDir (char part, EntryNum n, Directory &d); // argument je naziv fajla sa apsolutnom putanjom
	static char doesExist (char* fname);
	static File* open (char* fname, char mode);
	static char deleteFile (char* fname);

protected:
	FS ();
	static KernelFS *myImpl;

};
