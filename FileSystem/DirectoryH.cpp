#include "DirectoryH.h"
#include <iostream>

// Ucitava cluster direktorijuma sa diska
bool fsImpl::DirectoryHandler::loadCluster (int num) {

	char
		firstLevel[ClusterSize],
		secondLevel[ClusterSize];

	part->readCluster (bitVec->size, firstLevel);

	if (num >= (ClusterSize / 8)) {

		int
			fl = (num - 256) / 512 + 256,
			sl = (num - 256) % 512;

		location = ((ClusterNo*)firstLevel)[fl];

		if (location == 0) { return false; }

		part->readCluster (location, secondLevel);
		location = ((ClusterNo*)secondLevel)[sl];

		if (location == 0) { return false; }

		part->readCluster (location, (char *)entries);

	} else {

		location = ((ClusterNo*)firstLevel)[num];

		if (location == 0) { return false; }

		part->readCluster (location, (char *)entries);

	}

	return true;

}

// Smjesta cluster direktorijuma na disk
void fsImpl::DirectoryHandler::storeCluster () {

	part->writeCluster (location, (char *)entries);

}

// Dodaje novi cluster u direktorijum
void fsImpl::DirectoryHandler::appendCluster () {

	char
		clusterBuffer[ClusterSize],
		zeroSpace[ClusterSize] = {};

	location = bitVec->reserveNext ();
	part->writeCluster (location, zeroSpace);
	std::memcpy (entries, zeroSpace, ClusterSize);
	part->readCluster (bitVec->size, clusterBuffer);

	int
		i = 0;

	for (; ((ClusterNo*)clusterBuffer)[i] != 0; i++);

	if (i > 256) {

		char
			secondLevelBuffer[ClusterSize];

		part->readCluster (((ClusterNo*)clusterBuffer)[i - 1], secondLevelBuffer);

		for (int j = 0; j < 512; j++) {

			if (((ClusterNo*)secondLevelBuffer)[j] == 0) {

				((ClusterNo*)secondLevelBuffer)[j] = location;
				part->writeCluster (((ClusterNo*)clusterBuffer)[i - 1], secondLevelBuffer);
				return;

			}

		}

	}

	if (i >= 256) {

		*(ClusterNo*)zeroSpace = location;
		((ClusterNo*)clusterBuffer)[i] = bitVec->reserveNext ();
		part->writeCluster (((ClusterNo*)clusterBuffer)[i], zeroSpace);
		part->writeCluster (bitVec->size, clusterBuffer);
		return;

	}

	((ClusterNo*)clusterBuffer)[i] = location;
	part->writeCluster (bitVec->size, clusterBuffer);
	return;

}
