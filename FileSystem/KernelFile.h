#pragma once
#include "FileH.h"
#include <string>

class File;

class KernelFile {

	KernelFile () {};
	~KernelFile ();
	char write (BytesCnt, char* buffer);
	BytesCnt read (BytesCnt, char* buffer);
	char seek (BytesCnt);
	BytesCnt filePos ();
	char eof ();
	BytesCnt getFileSize ();
	char truncate ();

	std::string absolutePath;
	KernelFS* myFS;
	long position;
	char mode;
	bool upToDate;

	fsImpl::FileHandler fileHand;

	friend class File;
	friend class KernelFS;

};
