#pragma once
#include "BitVector.h"
#include "fs.h"

namespace fsImpl {

	class EntryHandler;

	class DirectoryHandler {

		DirectoryHandler () {};
		bool loadCluster (int num);
		void storeCluster ();
		void appendCluster ();

		Entry entries[102] = {};
		char reserved[8] = {};
		ClusterNo location;
		BitVector* bitVec;
		Partition* part;

		friend class EntryHandler;
		friend class KernelFS;

	};

}
